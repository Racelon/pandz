classdef Plane
    properties(SetAccess = private)
        m, Ro1, P1, alfa, cx, X, k, teta, sintet, dV, dH, V, H, HorizontalMatrix, VerticalMatrix, VerticalHorizontalMatrix;
        overclockingTime;
        liftTime;
        ocAndLtime;
        AmountOfDataPointsH = 10;
        AmountOfDataPointsV = 10;
    end
    
    methods (Access = private)
        function obj = overclocking(obj)
            obj.overclockingTime = (obj.dV * obj.m) / (obj.P1 * cos(obj.alfa) - obj.X);
        end
        
        function obj = ocAndLifting(obj)
            obj.ocAndLtime = obj.dH / (obj.V * obj.sintet);
        end
        
        function obj = lifting(obj)
            obj.liftTime = obj.dH / (obj.V * sin(obj.teta));
        end
    end

    methods

        function obj = setPar(obj, weight, V, dV, dH, H1)
            obj.m = weight;
            obj.V = V;
            obj.dV = dV;
            obj.dH = dH;
            obj.Ro1 = -3.3e-26 * H1.^3 + 3.3e-09 * H1.^2 + -0.00011 * H1 + 1.2; 
            obj.P1 = 9e-15 * H1.^3 + 23 * H1.^2 + 32 * H1 + 1.5e+05;
            obj.alfa = (obj.m * 9.8 - 0.17 * obj.Ro1 * obj.V * obj.V/2)/(obj.P1 / 57.3 + 0.058 * obj.Ro1 * obj.V * obj.V/2);
            obj.cx = 0.0024 * (obj.alfa) + 0.018;
            obj.X = obj.cx * obj.Ro1 * obj.V * obj.V/2;
            obj.k = obj.dV / obj.dH;
            obj.teta = (obj.P1 - obj.X) * 57.3 / (obj.m * 9.8);
            obj.sintet = (obj.P1 * cos(obj.alfa) - obj.X) / (obj.m * (obj.k * obj.V + 9.8));
        end
        
        function obj = setH(obj, H)
            obj.H = H;
        end

        function obj = setV(obj, V)
            obj.V = V;
        end

        function H = getH(obj)
            H = obj.H;
        end
        
        function V = getV(obj)
            V = obj.V;
        end

        function H = getAffordableHeight(obj)
            H = -3.66e-06*obj.V.^2 + 0.01*obj.V + 1;
        end

        function Time = getLiftTime(plane)
            plane = lifting(plane);
            Time = plane.liftTime;
        end

        function Time = getOverclockingTime(plane)
            plane = overclocking(plane);
            Time = plane.overclockingTime;
        end

        function Time = getOcAndLtime(plane)
            plane = ocAndLifting(plane);
            Time = plane.ocAndLtime;
        end

        function createHorizontalMatrix(plane, V2)
            plane.HorizontalMatrix = zeros(plane.AmountOfDataPointsH, plane.AmountOfDataPointsV);
            for i = 1 : plane.AmountOfDataPointsH
                for j = 1 : (plane.AmountOfDataPointsV + 1)
                    if (plane.H > plane.getAffordableHeight)
                        plane.HorizontalMatrix(i, j) = plane.getOverclockingTime;
                        plane.setV = plane.V - plane.dV;
                    end
                    plane.setV = V2 - plane.dV;
                    plane.setH = plane.getH - plane.dH;
                end
            end
        end

        function createVerticalMatrix(plane, V2)
            plane.VerticalMatrix = zeros(plane.AmountOfDataPointsH, plane.AmountOfDataPointsV);
            for i = 1 : (plane.AmountOfDataPointsH)
                for j = 1 : (plane.AmountOfDataPointsV + 1)
                    if (plane.H > plane.getAffordableHeight)
                        plane.VerticalMatrix(i, j) = plane.getLiftingTime;
                        plane.V = plane.V - plane.dV;
                    end
                    plane.setV = V2 - plane.dV;
                    plane.setH = plane.H - plane.dH;
                end
            end
        end

        function createVerticalHorizontalMatrix(plane, V2)
            plane.VerticalHorizontalMatrix = zeros(plane.AmountOfDataPointsH, plane.AmountOfDataPointsV);
            for i = 1 : (plane.AmountOfDataPointsH)
                for j = 1 : (plane.AmountOfDataPointsV + 1)
                    if (plane.H > plane.getAffordableHeight)
                        plane.VerticalMatrix(i, j) = plane.getLiftingTime;
                        plane.V = plane.V - plane.dV;
                    end
                    plane.setV = V2 - plane.dV;
                    plane.setH = plane.H - plane.dH;
                end
            end
        end
    end
end
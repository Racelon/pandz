classdef origArguments
    %Calculations Summary of this class goes here
    %   Detailed explanation goes here
    properties
    end

    properties (SetAccess = private)
        Table;
    end
    
    methods
        function obj = origArguments()
            TP = 'pData.csv';
            obj.Table = origArguments.importCSV(TP);
        end

        function [x, y] = getApproximatedP(obj)
            [x, y] = CurveFit(obj.Table.H, obj.Table.P);
        end
        
        function [x, y] = getApproximatedRo(obj)
            [x, y] = CurveFit(obj.Table.H, obj.Table.Ro);
        end
        
        function [x, y] = getApproximatedCx(obj)
            alpha = rmmissing(obj.Table.alpha);
            Cx = rmmissing(obj.Table.Cx);
            [x, y] = CurveFit(alpha, Cx);
        end
        
        function [x, y] = getApproximatedCy(obj)
            alpha = rmmissing(obj.Table.alpha);
            Cy = rmmissing(obj.Table.Cy);
            [x, y] = CurveFit(alpha, Cy);
        end

        function [x, y] = getApproximatedH(obj)
            H = rmmissing(obj.Table.H_1);
            V = rmmissing(obj.Table.V);
            [x, y] = CurveFit(V, H);
        end
    end

    methods (Static)
        function Table = importCSV(TablePath)
            Table = readtable(TablePath);
        end
    end
end

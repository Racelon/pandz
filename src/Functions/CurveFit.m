function [X, Y] = CurveFit(x, y)
    xi = min(x):0.1:max(x);
    N = 2;
    k1 = polyfit(x, y, N);
    y2 = 0;
    for k=0:N
        y2 = y2 + k1(N-k+1) * xi.^k;
    end
    X = xi;
    Y = y2;
end

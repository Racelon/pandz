addpath('src/Classes/')
addpath('Params')
addpath('src/Functions/')
dV = 10; % m/s
mas = 47000; % kg
H1 = 5000; % m
dH=1000; % m
v1 = 320; % m/s
v2 = 720; % m/s
V = (v1+v2)/2;
%Argument = origArguments;
%[x, y] = getApproximatedH(Argument);
%plot(x, y)

tu134 = Plane;
tu134.setPar(mas, V, dV, dH, H1);



LT = tu134.getLiftTime
OT = tu134.getOverclockingTime
OAL = tu134.getOcAndLtime